﻿/* FILE NAME   : Notebook.cs
 * PURPOSE     : Notebook handle file.
 * PROGRAMMER  : Zaytsev Leonid.
 * LAST UPDATE : 15.07.2019.
 * NOTE        : 'lz5' project namespace.
 */

using System;

// Project namespace
namespace lz5
{
  // Notebook (singleton) manage class
  class Notebook
  {
    // List node manage class
    private class Node
    {
      public Person Data; // Data about the person

      /* Next and previous list element references */
      public Node Next;
      public Node Prev;

      /* Class constructor initialized object by all feelds.
       * ARGUMENTS:
       *   - initializing data:
       *       in person Data;
       *   - next and previous element references:
       *       in node Next, Prev;
       */
      public Node( in Person Data, in Node Next, in Node Prev )
      {
        this.Data = Data;
        this.Next = Next;
        this.Prev = Prev;
      } // End of 'node' function

      // Class default destructor
      ~Node()
      {
        Data.Clear();
        Next = null;
        Prev = null;
      } // End of '~node' function
    } // End of 'node' class

    private Node head, tail;  // The first and the last notebook elements
    public uint Size          // Count persons in the notebook
    { get; private set; }

    public Person this[uint Index]
    {
      get
      {
        Node Current = head;
        Node Previous = Current;

        int i = 0;

        /* Follow to the necessary person */
        while (i != Index)
        {
          /* "Remember" current person and go to the next */
          Previous = Current;
          Current = Current.Next;
          i++;
        }

        return Current.Data;
      }

      set
      {
        // If index value is more then elements count return
        if (Index > Size)
        {
          Console.WriteLine("Index is out of array.");
          IO.WaitAnyKey();
          return;
        }

        if (Index == 0)
        {
          push_front(value);
          return;
        }

        if (Index == Size)
        {
          push_back(value);
          return;
        }

        // If necessary person in the first half of notebook
        if (Index < Size / 2)
        {
          Node Current = head;
          Node Previous = Current;

          int i = 0;

          /* Follow to the necessary person */
          while (i != Index)
          {
            /* "Remember" current person and go to the next */
            Previous = Current;
            Current = Current.Next;
            i++;
          }

          Previous.Next = new Node(value, Current, Current.Prev);

          Current.Prev = Previous.Next;
        }
        else // If necessary person in the second half of notebook
        {
          Node Current = tail;
          Node Next = Current;

          int i = 0;

          /* Follow to the necessary person */
          while (i != Size - Index)
          {
            /* "Remember" current person and go to the previous */
            Next = Current;
            Current = Next.Prev;

            i++;
          }

          /* Set new node in the list */
          Next.Prev = new Node(value, Next, Current);
          Current.Next = Next.Prev;
        }
        Size++;
      } // End of set
    } // End of indexer

    private static Notebook Instance = new Notebook(); // Class single instance

    // Class default constructor
    private Notebook()
    {
    } // End of 'Notebook' function

    // Class default destructor
    ~Notebook()
    {
      while (head != null)
        Instance.pop_back();
    } // End of '~Notebook' function

    /* Get class instance function.
     * ARGUMENTS: None.
     * RETURNS:
     *   (Notebook) class instance.
     */
    public static Notebook GetInstance()
    {
      return Instance;
    } // End of 'GetInstance' function

    /* Check notebook for if notebook is empty function.
     * ARGUMENTS: None.
     * RETURNS:
     *   (bool) true - if empty, else - false.
     */
    public bool IsEmpty()
    {
      if (Size != 0)
        return false;

      return true;
    } // End of 'IsEmpty function

    /* Add person to the notebook start function.
     * ARGUMENTS:
     *   - person to add:
     *       in person NewPerson;
     * RETURNS: None.
     */
    public void push_front( in Person NewPerson )
    {
      Node NewNode = new Node(NewPerson, head, null);

      // If notebook isn't empty
      if (head != null)
      {
        head.Prev = NewNode;
        head = NewNode;
      }
      else // If notebook is empty
      {
        head = NewNode;
        tail = NewNode;
      }

      Size++;
    } // End of 'push_front' function

    /* Add person to the notebook back function.
     * ARGUMENTS:
     *   - person to add:
     *       in person NewPerson;
     * RETURNS: None.
     */
    public void push_back( in Person NewPerson )
    {
      Node NewNode = new Node(NewPerson, null, tail);

      // If notebook isn't empty
      if (head != null)
      {
        tail.Next = NewNode;
        tail = tail.Next;
      }
      else // If notebook is empty
      {
        head = NewNode;
        tail = NewNode;
      }

      Size++;
    } // End of 'push_back' function

    /* Delete person from the notebook start function.
     * ARGUMENTS: None.
     * RETURNS: None.
     */
    public void pop_front()
    {
      // If only one person in the notebook
      if (head.Next == null)
      {
        head = null;
        Size--;
        return;
      }

      head = head.Next;
      head.Prev = null;

      Size--;
    } // End of 'pop_front' function

    /* Delete person from the notebook back function.
     * ARGUMENTS: None.
     * RETURNS: None.
     */
    public void pop_back()
    {
      // If only one person in the notebook
      if (head.Next == null)
      {
        head = null;
        Size--;
        return;
      }

      tail = tail.Prev;
      tail.Next = null;

      Size--;
    } // End of 'pop_back' function

    /* Delete person from the notebook by index.
     * ARGUMENTS:
     *   - index of person to delete:
     *       int Index;
     * RETURNS: None.
     */
    public void pop_index( int Index )
    {
      if (head == null)
      {
        Console.WriteLine("Notebook is empty.");
        return;
      }

      if (Index == 0)
      {
        pop_front();
        return;
      }

      if (Index == Size - 1)
      {
        pop_back();
        return;
      }

      Node Current = head;
      Node Previous = Current;

      int i = 0;

      /* Follow to the necessary person */
      while (i != Index)
      {
        /* "Remember" current person and go to the next */
        Previous = Current;
        Current = Current.Next;
        i++;
      }

      Current = null;
      Previous.Next = Previous.Next.Next;
      Previous.Next.Prev = Previous;

      Size--;
    } // End of 'pop_index' function

    /* Output notebook on the screen function.
     * ARGUMENTS: None.
     * RETURNS: None.
     */
    public void Display()
    {
      if (head != null)
      {
        uint i = 1;
        Node Current = new Node(head.Data, head.Next, null);

        // Follow for all elements
        do
        {
          Console.WriteLine(IO.EndLine);
          Console.WriteLine("The " + $"{i}" + (i > 3 ? "th" : i > 2 ? "rd" : i > 1 ? "nd" : "st"));
          i++;
          Current.Data.Display();
          Current = Current.Next;
        } while (Current != null);

        Console.WriteLine(IO.EndLine);
      }
      else
        Console.WriteLine("Notebook is empty.");
    } // End of 'Display' function

    /* Output notebook on the screen by index function.
     * ARGUMENTS: None.
     * RETURNS: None.
     */
    public void Display( int Index )
    {
      // If notebook isn't empty
      if (head != null)
      {
        int i = 0;

        Console.WriteLine(IO.EndLine);

        Node Current = head;

        // Follow for the necessary person
        while (i != Index)
        {
          Current = Current.Next;
          i++;
        }
        Current.Data.Display();
        Console.WriteLine(IO.EndLine);
      }
      else
        Console.WriteLine("Notebook is empty.");
    } // End of 'Display' function

    /* Get person by index function.
     * ARGUMENTS:
     *   - person index in the notebook:
     *       uint Index;
     * RETURNS:
     *   (ref person) reference to the necessary person.
     */
    public ref Person GetByIndex( uint Index )
    {
      Node Current = head;
      Node Previous;

      int i = 0;

      /* Follow to the necessary person */
      while (i != Index)
      {
        /* "Remember" current person and go to the next */
        Previous = Current;
        Current = Current.Next;
        i++;
      }

      return ref Current.Data;
    } // End of 'GetByIndex' function
  } // End of 'Notebook' class
} // end of 'lz5' namespace

// END OF 'Notebook.cs' FILE
