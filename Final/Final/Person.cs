﻿/* FILE NAME   : Person.cs
 * PURPOSE     : Person data handle file.
 * PROGRAMMER  : Zaytsev Leonid.
 * LAST UPDATE : 15.07.2019.
 * NOTE        : 'lz5' project namespace.
 */

using System;

// Project namespace
namespace lz5
{
  // Person handle structure
  struct Person
  {
    // Date storage structure
    public struct Date
    {
      public int Day;
      public int Month;
      public int Year;
    } // End of 'Date' struct

    // Person fullname
    public string Surname
    { get; set; }
    public string Name
    { get; set; }
    public string SecondName
    { get; set; }

    public string Phone          // Person phone number
    { get; set; }

    public string Country        // Person location
    { get; set; }

    private Date bDay;            // Person birthday

    // Person working state
    public string Company
    { get; set; }
    public string Position
    { get; set; }

    public string Other          // Other information about person
    { get; set; }

    /* Class constructor by all params.
     * ARGUMENTS:
     *   - person init name:
     *       in string Surname, in string Name, in string SecondName;
     *   - person init phone number:
     *       in string Phone;
     *   - person init location:
     *       in string Country;
     *   - person init birthday:
     *       in int Day, in int Month, in int Year;
     *   - person init working state:
     *       in string Company, in string Position;
     *   - init other information about person:
     *       in string Other;
     */
    public Person( in string Surname, in string Name, in string SecondName, in string Phone, in string Country,
      in int Day, in int Month, in int Year, in string Company, in string Position, in string Other )
    {
      this.Surname = Surname;
      this.Name = Name;
      this.SecondName = SecondName;
      this.Phone = Phone;
      this.Country = Country;
      this.bDay.Day = Day;
      this.bDay.Month = Month;
      this.bDay.Year = Year;
      this.Company = Company;
      this.Position = Position;
      this.Other = Other;
    } // End of 'person' function

    /* Set new birthday function.
     * ARGUMENTS:
     *   - new birthday date:
     *       in int Day, Month, Year;
     * RETURNS: None.
     */
    public void SetBirthday( in int Day, in int Month, in int Year )
    {
      this.bDay.Day = Day;
      this.bDay.Month = Month;
      this.bDay.Year = Year;
    } // End of 'SetBirthday' function

    /* Output person data on the screen function.
     * ARGUMENTS: None.
     * RETURNS: None.
     */
    public void Display()
    {
      Console.WriteLine($"Surname: {Surname}");
      Console.WriteLine($"Name: {Name}");
      Console.WriteLine($"The second name: {SecondName}");
      Console.WriteLine($"Phone: {Phone}");
      Console.WriteLine($"Country: {Country}");
      Console.WriteLine($"Birthday: {bDay.Day}." + (bDay.Month < 10 ? "0" : "") + $"{bDay.Month}.{bDay.Year}");
      Console.WriteLine($"Company: {Company}");
      Console.WriteLine($"Position: {Position}");
      Console.WriteLine($"Other: {Other}");
    } // End of 'Display' function

    /* Clear person data function.
     * ARGUMENTS: None.
     * RETURNS: None.
     */
    public void Clear()
    {
      this.Surname = null;
      this.Name = null;
      this.SecondName = null;
      this.Phone = null;
      this.Country = null;
      this.bDay.Day = 0;
      this.bDay.Month = 0;
      this.bDay.Year = 0;
      this.Company = null;
      this.Position = null;
      this.Other = null;
    } // End of 'Clear' function
  } // End of 'person' struct
} // end of 'lz5' namespace

// END OF 'Person.cs' FILE
