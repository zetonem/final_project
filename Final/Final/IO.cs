﻿/* FILE NAME   : IO.cs
 * PURPOSE     : Program input-output system handle file.
 * PROGRAMMER  : Zaytsev Leonid.
 * LAST UPDATE : 17.07.2019.
 * NOTE        : 'lz5' project namespace.
 */

using System;

// Project namespace
namespace lz5
{
  // Input-output system handle class
  static class IO
  {
    public static string EndLine = "================================================================================";
    private const string agreement = "yes";  // Agreement value
    private const string denial = "no";      // Denial value
    private const string EmptyField = "N/A"; // Value for empty preson field

    /* Keystroke waiting function.
     * ARGUMENTS: None.
     * RETURNS: None.
     */
    public static void WaitAnyKey()
    {
      Console.WriteLine("Press any key to continue.");
      Console.ReadKey();
    } // End of 'WaitAnyKey' function

    /* Get correct data from the console function.
     * ARGUMENTS:
     *   - input variable:
     *       out int Data;
     *   - variable name:
     *       string DataName;
     *   - exception message:
     *       string ExceptionMessage;
     * RETURNS:
     *   (bool) true - if correct data get, else - false.
     */
    private static bool GetCorrectData( out int Data, string DataName = "Input value",
      string ExceptionMessage = "Wrong data format! Do you want to retry?" )
    {
      bool IsCorrect = false;
      Data = 0;

      // Do while input data wouldn't be correct
      do
      {
        Console.Write(DataName + ": ");
        try
        {
          Data = Convert.ToInt32(Console.ReadLine());
          IsCorrect = true;
        }
        catch (FormatException)
        {
          Console.WriteLine(ExceptionMessage);
          Console.WriteLine(agreement + "/" + denial);
          string Str = Console.ReadLine().ToLower(); // Choice string

          if (Str == denial)
            return false;
        }
      } while (!IsCorrect);

      return true;
    } // End of 'GetCorrectData' function

    /* Get correct data from the console function.
     * ARGUMENTS:
     *   - input variable:
     *       out string Data;
     *   - variable name:
     *       string DataName;
     * RETURNS:
     *   (bool) true - if correct data get, else - false.
     */
    private static bool GetCorrectData( out string Data, string DataName = "param" )
    {
      int Choice;
      Data = "";

      Console.Write("Input person " + DataName + ": ");
      Data = Console.ReadLine();
      while (Data.Length == 0)
      {
        Console.WriteLine("Thers is no person without " + DataName + "!");
        Console.WriteLine("1 - Retry to input " + DataName + ".\n2 - Get back.");
        GetCorrectData(out Choice, "");

        switch (Choice)
        {
          case 1:
            Console.Write("Input person " + DataName + ": ");
            Data = Console.ReadLine();
            break;
          case 2:
            return false;
          default:
            Console.WriteLine("Wrong choice!");
            break;
        }
      }

      return true;
    } // End of 'GetCorrectData' function

    /* Try to catch an exception while reading data value from the console function.
     * ARGUMENTS:
     *   - read data:
     *       ref int Data;
     *   - output message if exception:
     *       string ExceptionMessage;
     */
    private static bool IsInputException<Exception>( ref int Data,
      string ExceptionMessage = "Wrong data format!" )
    {
      try
      {
        Data = Convert.ToInt32(Console.ReadLine());
      }
      catch (FormatException)
      {
        Console.WriteLine("Wrong data format!");
        WaitAnyKey();
        Console.Clear();

        return true;
      }

      return false;
    } // End of 'IsInputException' function

    /* Get information about new person from the console and add it to the notebook function.
     * ARGUMENTS: None.
     * RETURNS: None.
     */
    private static void AddNewPerson()
    {
      string Surname, Name, SecondName, Phone, Country, Company, Position, Other;
      int Day = 0, Month = 0, Year = 0;

      if (!GetCorrectData(out Surname, "surname"))
        return;

      if (!GetCorrectData(out Name, "name"))
        return;

      Console.Write("Input person second name: ");
      SecondName = Console.ReadLine();
      if (SecondName.Length == 0)
        SecondName = EmptyField;

      Console.Write("Input person phone: ");
      Phone = Console.ReadLine();
      if (Phone.Length == 0)
        Phone = EmptyField;

      if (!GetCorrectData(out Country, "country"))
        return;

      Console.Write("Input person birthday: ");

      if (!GetCorrectData(out Day, "Day"))
        return;

      if (!GetCorrectData(out Month, "Month"))
        return;

      if (!GetCorrectData(out Year, "Year"))
        return;

      Console.Write("Input person company: ");
      Company = Console.ReadLine();
      if (Company.Length != 0)
      {
        Console.Write("Input person poistion in this company: ");
        Position = Console.ReadLine();
        if (Position.Length == 0)
          Position = EmptyField;
      }
      else
      {
        Company = EmptyField;
        Position = EmptyField;
      }

      Console.Write("Input other information about a person: ");
      Other = Console.ReadLine();
      if (Other.Length == 0)
        Other = "N/A";

      Console.Clear();

      Console.Write($"Input index to add to the notebook (from 1 to {Notebook.GetInstance().Size + 1}): ");

      uint Index;

      try
      {
        Index = Convert.ToUInt32(Console.ReadLine());
      }
      catch (FormatException)
      {
        Console.WriteLine("Wrong index input. Person will be added to the notebook back.");
        Index = Notebook.GetInstance().Size + 1;
        WaitAnyKey();
      }
      catch (OverflowException)
      {
        Console.WriteLine("Wrong index input. Person will be added to the notebook back.");
        Index = Notebook.GetInstance().Size + 1;
        WaitAnyKey();
      }

      Notebook.GetInstance()[Index - 1] = new Person(Surname, Name, SecondName, Phone, Country, Day, Month, Year, Company, Position, Other);
    } // End of 'AddNewPerson' function

    /* Delete person from the notebook function.
     * ARGUMENTS: None.
     * RETURNS: None.
     */
    private static void DeletePerson()
    {
      if (Notebook.GetInstance().IsEmpty())
      {
        Console.WriteLine("Notebook is empty.");
        WaitAnyKey();
        return;
      }

      Console.Write($"Input index (from 1 to {Notebook.GetInstance().Size}): ");

      int Choice = 0;
      if (IsInputException<FormatException>(ref Choice))
        return;

      Console.Clear();

      Console.WriteLine("Are you sure to delete this person:");
      Notebook.GetInstance().Display(Choice - 1);
      Console.WriteLine(agreement + "/" + denial);
      string Str = Console.ReadLine();

      if (Str == denial)
        return;

      Notebook.GetInstance().pop_index(Choice - 1);
    } // End of 'DeletePerson' function

    /* Output one person from the notebook on the screen function.
     * ARGUMENTS: None.
     * RETURNS: None.
     */
    private static void OutputPerson()
    {
      if (Notebook.GetInstance().IsEmpty())
      {
        Console.WriteLine("Notebook is empty.");
        WaitAnyKey();
        return;
      }

      Console.Write($"Input index (from 1 to {Notebook.GetInstance().Size}): ");

      int Choice = 0;
      if (IsInputException<FormatException>(ref Choice))
        return;

      Notebook.GetInstance().Display(Choice - 1);

      WaitAnyKey();
    } // End of 'OutputPerson' function

    /* Edit person data function.
     * ARGUMENTS: None.
     * RETURNS: None.
     */
    private static void EditPerson()
    {
      Console.WriteLine("0 - Get back.\n" +
        "1 - Edit surname.\n" +
        "2 - Edit name.\n" +
        "3 - Edit second name.\n" +
        "4 - Edit phone.\n" +
        "5 - Edit country.\n" +
        "6 - Edit birthday.\n" +
        "7 - Edit company.\n" +
        "8 - Edit position.\n" +
        "9 - Edit other");

      int Choice = 0;

      if (IsInputException<FormatException>(ref Choice))
        return;

      Console.Write($"Input index (from 1 to {Notebook.GetInstance().Size}): ");

      int Index = 0;

      if (IsInputException<FormatException>(ref Index))
        return;

      uint UIndex = (uint)Index - 1;

      string NewDataStr;

      Console.Clear();

      switch (Choice)
      {
        case 0:
          return;
        case 1:
          if (!GetCorrectData(out NewDataStr, "surname"))
            return;
          Notebook.GetInstance().GetByIndex(UIndex).Surname = NewDataStr;
          Console.Clear();
          break;
        case 2:
          if (!GetCorrectData(out NewDataStr, "name"))
            return;
          Notebook.GetInstance().GetByIndex(UIndex).Name = NewDataStr;
          break;
        case 3:
          if (!GetCorrectData(out NewDataStr, "second name"))
            return;
          Notebook.GetInstance().GetByIndex(UIndex).SecondName = NewDataStr;
          break;
        case 4:
          if (!GetCorrectData(out NewDataStr, "phone"))
            return;
          Notebook.GetInstance().GetByIndex(UIndex).Phone = NewDataStr;
          break;
        case 5:
          if (!GetCorrectData(out NewDataStr, "country"))
            return;
          Notebook.GetInstance().GetByIndex(UIndex).Country = NewDataStr;
          break;
        case 6:
          {
            int Day, Month, Year;

            if (!GetCorrectData(out Day, "day"))
              return;
            if (!GetCorrectData(out Month, "month"))
              return;
            if (!GetCorrectData(out Year, "year"))
              return;
            Notebook.GetInstance().GetByIndex(UIndex).SetBirthday(Day, Month, Year);
            break;
          }
        case 7:
          if (!GetCorrectData(out NewDataStr, "company"))
            return;
          Notebook.GetInstance().GetByIndex(UIndex).Company = NewDataStr;
          break;
        case 8:
          if (Notebook.GetInstance().GetByIndex(UIndex).Company.Length == 0)
            Console.WriteLine("This person hasn't got any job.");
          if (!GetCorrectData(out NewDataStr, "position"))
            return;
          Notebook.GetInstance().GetByIndex(UIndex).Position = NewDataStr;
          break;
        case 9:
          if (!GetCorrectData(out NewDataStr, "other"))
            return;
          Notebook.GetInstance().GetByIndex(UIndex).Position = NewDataStr;
          break;
      }
    } // End of 'EditPerson' function

    /* Menu output and manage function.
     * ARGUMENTS: None.
     * RETURNS: None.
     */
    public static void Menu()
    {
      bool IsExit = false; // Program exit status

      while (!IsExit)
      {
        int Choice = 0;

        do
        {
          Console.WriteLine("0 - Exit.\n" +
            "1 - Add new person to the notebook.\n" +
            "2 - Edit person data.\n" +
            "3 - Delete person from the notebook.\n" +
            "4 - Show a person data.\n" +
            "5 - Show all notebook members.");
        } while (IsInputException<FormatException>(ref Choice));

        Console.Clear();

        switch (Choice)
        {
          case 0:
            IsExit = !IsExit;
            break;
          case 1:
            AddNewPerson();
            Console.Clear();
            break;
          case 2:
            EditPerson();
            Console.Clear();
            break;
          case 3:
            DeletePerson();
            Console.Clear();
            break;
          case 4:
            OutputPerson();
            Console.Clear();
            break;
          case 5:
            Console.Clear();
            Notebook.GetInstance().Display();
            WaitAnyKey();
            Console.Clear();
            break;
        }
      }
    } // End of 'Menu' function
  } // End of 'IO' class
} // end of 'lz5' namespace

// END OF 'IO.cs' FILE
