﻿/* FILE NAME   : Program.cs
 * PURPOSE     : The main program file.
 * PROGRAMMER  : Zaytsev Leonid.
 * LAST UPDATE : 15.07.2019.
 * NOTE        : 'lz5' project namespace.
 */

using System;

// Project namespace
namespace lz5
{
  // The main program class
  class Program
  {
    /* The main program function.
     * ARGUMENTS:
     *   - program input data:
     *       string[] args;
     * RETURNS: None.
     */
    static void Main( string[] args )
    {
      IO.Menu();
    } // End of 'Main' function
  } // End of 'Program' class
} // end of 'lz5' namespace

// END OF 'Program.cs' FILE
